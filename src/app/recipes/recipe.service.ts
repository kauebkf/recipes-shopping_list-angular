import {Injectable, EventEmitter} from "@angular/core";
import {Recipe} from "./recipe.model";
import {Ingredient} from "../shared/ingredient.model";

@Injectable({providedIn: "root"})
export class RecipeService {

  private recipes: Recipe[] = [
    new Recipe(
      0,
      'Wild Tropical Shrimps',
      'Shrimps like you never saw before!',
      'https://upload.wikimedia.org/wikipedia/commons/3/39/Recipe.jpg',
      [
        new Ingredient('Shrimp', 17),
        new Ingredient('Aspargus', 5)
      ]
    ),
    new Recipe(
      1,
      'Butter Onion Steak',
      'This is just incredible!',
      'https://cdn.pixabay.com/photo/2016/06/15/19/09/food-1459693_960_720.jpg',
      [
        new Ingredient('Beef', 3),
        new Ingredient('Onions', 1)
      ]
    )
  ];

  getRecipes() {
    // JavaScript by default returns a reference to the object, meaning that the object could
    // be modified from outside. calling slice() without arguments returns a copy of the object,
    // preserving our encapsulation.
    return this.recipes.slice();
  }

  getRecipe(id: number): Recipe {
    return this.recipes.find(recipe => recipe.id === id);
  }
}
