import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {RecipeDetailComponent} from "./recipe-detail/recipe-detail.component";
import {RecipeDefaultComponent} from "./recipe-default/recipe-default.component";

const recipeRoutes: Routes = [
  { path: '', component: RecipeDefaultComponent },
  { path: ':id', component: RecipeDetailComponent }
]

@NgModule({
    imports: [RouterModule.forChild(recipeRoutes)],
    exports: [RouterModule]
  })
export class RecipeRoutingModule {}
