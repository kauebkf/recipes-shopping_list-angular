import {Injectable,} from "@angular/core";
import {Ingredient} from "../shared/ingredient.model";
import {Subject} from "rxjs";

@Injectable({providedIn: "root"})
export class ShoppingListService {
  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Tomatoes', 10)
  ];
  updatedIngredients = new Subject<Ingredient[]>();

  getIngredients() {
    return this.ingredients.slice();
  }

  addIngredients(ingredient: Ingredient[]) {
    ingredient.map(ingredientItem => {
      this.ingredients.push(ingredientItem);
      this.updatedIngredients.next(this.ingredients.slice());
    });
  }
}
